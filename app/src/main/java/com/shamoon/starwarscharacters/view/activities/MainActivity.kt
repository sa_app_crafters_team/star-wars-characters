package com.shamoon.starwarscharacters.view.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.shamoon.starwarscharacters.R
import com.shamoon.starwarscharacters.service.model.Result
import com.shamoon.starwarscharacters.view.fragments.character.CharacterFragment
import com.shamoon.starwarscharacters.view.fragments.characters.CharactersFragment

class MainActivity : AppCompatActivity(), CharactersFragment.CharactersListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(
                    R.id.container,
                    CharactersFragment.newInstance(),
                    CharactersFragment::class.java.simpleName
                )
                .commitNow()
        }
    }

    override fun onCharacterClick(user: Result) {
        //from here I will go to next list of recipes
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, CharacterFragment.newInstance(user))
            .addToBackStack(CharacterFragment::class.java.simpleName)
            .commit()
    }

}