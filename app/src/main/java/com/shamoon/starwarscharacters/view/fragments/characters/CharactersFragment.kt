package com.shamoon.starwarscharacters.view.fragments.characters

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.shamoon.starwarscharacters.R
import com.shamoon.starwarscharacters.databinding.CharactersFragmentBinding
import com.shamoon.starwarscharacters.service.model.Result
import com.shamoon.starwarscharacters.utils.Utils

class CharactersFragment : Fragment() {


    companion object {
        fun newInstance() = CharactersFragment()
    }

    private lateinit var viewModel: CharactersViewModel
    private lateinit var binding: CharactersFragmentBinding
    private lateinit var mListener: CharactersListener

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.characters_fragment, container, false)
        if (activity?.title!=null)
            activity!!.title = "Star Wars Characters"
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupBindings(savedInstanceState)
    }

    private fun setupBindings(savedInstanceState: Bundle?) {
        viewModel = ViewModelProvider(this).get(CharactersViewModel::class.java)

        if (savedInstanceState == null) {
            viewModel.init()
        }
        viewModel.context = context!!
        binding.model = viewModel
        setupListUpdate()
    }

    private fun setupListUpdate() {
        viewModel.loading.set(VISIBLE)
        viewModel.setupList()

        val observer = Observer<ArrayList<Result>> { response ->
            // Update the UI, in this case, a TextView.
            viewModel.loading.set(GONE)
            if (response==null || response.size == 0) {
                viewModel.showEmpty.set(VISIBLE)
            } else {
                viewModel.showEmpty.set(GONE)
                viewModel.setCharactersInAdapter(response)
            }
        }

        viewModel.getCharacters().observe(viewLifecycleOwner, observer)

        setupListClick();

    }

    private fun setupListClick() {
        val observer = Observer<Result> { character ->
            if (character != null) {
                mListener.onCharacterClick(character)
            }
        }
        viewModel.selected.observe(viewLifecycleOwner, observer)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is CharactersListener) {
            mListener = context as CharactersListener
        }
    }

    interface CharactersListener {
        fun onCharacterClick(user: Result)
    }

}