package com.shamoon.starwarscharacters.view.fragments.characters

import android.content.Context
import android.view.View
import androidx.databinding.ObservableInt
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.shamoon.starwarscharacters.R
import com.shamoon.starwarscharacters.service.model.Result
import com.shamoon.starwarscharacters.service.repositories.CharactersRepository
import com.shamoon.starwarscharacters.utils.Utils
import com.shamoon.starwarscharacters.view.adapters.CharactersListAdapter

class CharactersViewModel : ViewModel() {
    lateinit var loading: ObservableInt
    lateinit var showEmpty: ObservableInt
    private var errorMessage: String = ""
    private lateinit var charactersRepository: CharactersRepository
    private lateinit var adapter: CharactersListAdapter
    lateinit var selected: MutableLiveData<Result>
    lateinit var context: Context

    fun init() {
        charactersRepository =
            CharactersRepository()
        adapter = CharactersListAdapter(R.layout.character_list_item, this)
        loading = ObservableInt(View.GONE)
        showEmpty = ObservableInt(View.GONE)
        selected = MutableLiveData()
        errorMessage = ""
    }

    fun fetchList() {
        charactersRepository.fetchList()
    }

    fun tryAgain(){
        setupList()
    }

    fun getErrorMessage(): String {
        if (charactersRepository.charactersResponse.value != null && charactersRepository.charactersResponse.value?.detail!=null){
            errorMessage = charactersRepository.charactersResponse.value!!.detail!!
        }
        if (errorMessage == ""){
            errorMessage = "Check your internet connection."
        }

        return  errorMessage
    }

    fun setupList() {
        loading.set(View.VISIBLE)
        showEmpty.set(View.GONE)
        if (Utils().isOnline(context)){
            fetchList()
        }else{
            loading.set(View.GONE)
            showEmpty.set(View.VISIBLE)
            setErrorMessage("Check your internet connection")
        }

    }



    fun setErrorMessage(message: String){
        this.errorMessage = message
    }


    fun getCharacters(): MutableLiveData<ArrayList<Result>> {
        return charactersRepository.characters
    }

    fun getAdapter(): CharactersListAdapter? {
        return adapter
    }

    fun setCharactersInAdapter(characters: ArrayList<Result>) {
        adapter.setCharacters(characters)
        adapter.notifyDataSetChanged()
    }

    fun onItemClick(index: Int?) {
        val db: Result? = getCharacterAt(index)
        selected.setValue(db)
    }

    fun getCharacterAt(index: Int?): Result? {
        return if (charactersRepository.characters
                .value != null && index != null && charactersRepository.characters
                .value!!.size > index
        ) {
            charactersRepository.characters.getValue()!!.get(index)
        } else null
    }


}