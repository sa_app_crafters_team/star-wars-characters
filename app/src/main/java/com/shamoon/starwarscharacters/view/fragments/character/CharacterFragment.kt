package com.shamoon.starwarscharacters.view.fragments.character

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelStoreOwner
import com.shamoon.starwarscharacters.R
import com.shamoon.starwarscharacters.databinding.CharacterFragmentBinding
import com.shamoon.starwarscharacters.service.model.Result

class CharacterFragment : Fragment() {

    companion object {
        fun newInstance() = CharacterFragment()

        @JvmStatic
        fun newInstance(character: Result) = CharacterFragment().apply {
            arguments = Bundle().apply {
                putSerializable("character", character)
            }
        }
    }

    private lateinit var viewModel: CharacterViewModel
    private lateinit var character: Result
    private lateinit var binding: CharacterFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.character_fragment, container, false)
        activity!!.title = "Star Wars Character"

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupBindings(savedInstanceState)
    }

    private fun setupBindings(savedInstanceState: Bundle?) {
        viewModel = ViewModelProvider(this).get(CharacterViewModel::class.java)

        if (savedInstanceState == null) {
            viewModel.init(character);
        } else {
            viewModel.setCharacter(character)
        }
        binding.characterModel = viewModel
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        arguments?.getSerializable("character")?.let {
            character = it as Result
        }
    }


}