package com.shamoon.starwarscharacters.view.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.shamoon.starwarscharacters.BR
import com.shamoon.starwarscharacters.databinding.CharacterListItemBinding
import com.shamoon.starwarscharacters.service.model.Result
import com.shamoon.starwarscharacters.view.fragments.characters.CharactersViewModel

class CharactersListAdapter : RecyclerView.Adapter<CharactersListAdapter.GenericViewHolder> {
    private var layoutId: Int
    private var characters: ArrayList<Result> = mutableListOf<Result>() as ArrayList<Result>
    private var viewModel: CharactersViewModel

    constructor(
        @LayoutRes layoutId: Int,
        viewModel: CharactersViewModel?
    ) {
        this.layoutId = layoutId
        this.viewModel = viewModel!!
    }

    class GenericViewHolder : RecyclerView.ViewHolder {
        var binding: CharacterListItemBinding

        constructor(binding: CharacterListItemBinding) : super(binding.root) {
            this.binding = binding
        }

        fun bind(viewModel: CharactersViewModel?, position: Int?) {
            binding.setVariable(BR.position, position)
            binding.setVariable(BR.viewModelCharacterItem, viewModel)
            binding.executePendingBindings()
        }
    }

    fun setCharacters(categories: ArrayList<Result>) {
        this.characters.clear()
        this.characters.addAll(categories)
    }

    private fun getLayoutIdForPosition(position: Int): Int {
        return layoutId
    }

    override fun getItemViewType(position: Int): Int {
        return getLayoutIdForPosition(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenericViewHolder {
        val binding = DataBindingUtil.inflate<CharacterListItemBinding>(
            LayoutInflater.from(parent.context),
            viewType,
            parent,
            false
        )

        return GenericViewHolder(binding)
    }

    override fun onBindViewHolder(holder: GenericViewHolder, position: Int) {
        holder.bind(viewModel, position)
    }

    override fun getItemCount(): Int {
        return characters.size
    }
}