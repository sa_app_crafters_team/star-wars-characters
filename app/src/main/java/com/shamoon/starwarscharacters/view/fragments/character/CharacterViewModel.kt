package com.shamoon.starwarscharacters.view.fragments.character

import android.util.Log
import androidx.lifecycle.ViewModel
import com.shamoon.starwarscharacters.service.model.Result
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class CharacterViewModel : ViewModel() {
    private lateinit var character: Result

    fun init(character: Result){
        this.character = character
    }

    fun setCharacter(character: Result) {
        this.character = character
    }

    fun getCharacter(): Result {
        return character
    }

    @Throws(ParseException::class)
    fun iso8601Format(): String? {
         try {
             val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
             val formatter = SimpleDateFormat("dd-MM-yyyy")
             val output = formatter.format(parser.parse(character.created))
             return output
        } catch (ex: IllegalArgumentException) {
            // error happen in Java 6: Unknown pattern character 'X'
            return character.created
        }
    }

    fun getCreatedDate(): String {
        return iso8601Format().toString()

    }
}