package com.shamoon.starwarscharacters.service.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class StarWarsCharactersResponse {
    @SerializedName("count")
    @Expose
    var count: Int? = null

    @SerializedName("detail")
    @Expose
    var detail: String? = null

    @SerializedName("next")
    @Expose
    var next: String? = null

    @SerializedName("previous")
    @Expose
    var previous: Any? = null

    @SerializedName("results")
    @Expose
    var results: List<Result>? = null
}