package com.shamoon.starwarscharacters.service.repositories

import android.util.Log
import androidx.databinding.BaseObservable
import androidx.lifecycle.MutableLiveData
import com.shamoon.starwarscharacters.service.Api
import com.shamoon.starwarscharacters.service.model.Result
import com.shamoon.starwarscharacters.service.model.StarWarsCharactersResponse
import com.shamoon.starwarscharacters.utils.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CharactersRepository : BaseObservable() {
    private var charactersList: ArrayList<Result> = ArrayList()
    val charactersResponse: MutableLiveData<StarWarsCharactersResponse> by lazy {
        MutableLiveData<StarWarsCharactersResponse>()
    }

    val characters: MutableLiveData<ArrayList<Result>> by lazy {
        MutableLiveData<ArrayList<Result>>()
    }
    val page: MutableLiveData<Int> by lazy {
        MutableLiveData<Int>()
    }

    fun fetchList() {

        val callback: Callback<StarWarsCharactersResponse?> =
            object : Callback<StarWarsCharactersResponse?> {
                override fun onFailure(call: Call<StarWarsCharactersResponse?>, t: Throwable) {
                    Log.e("RecipeCategory", t.message, t)
                    charactersResponse.value?.detail = "Server is not responding"
                }

                override fun onResponse(
                    call: Call<StarWarsCharactersResponse?>,
                    response: Response<StarWarsCharactersResponse?>
                ) {
                    if (response.isSuccessful) {
                        val body: StarWarsCharactersResponse = response.body()!!
                        if (charactersResponse.value == null) {
                            charactersResponse.value = body

                            charactersList = body.results as ArrayList<Result>
                            if (body.next != null) {
                                val arr: Array<String> = charactersResponse.value!!.next!!.split("=").toTypedArray()
                                page.value = arr.last().toString().toInt()
                                fetchList()
                            }else{
                                characters.value = charactersList
                            }
                        } else {
                            if (body.count != null)
                                charactersResponse.value?.count = body.count
                            if (body.detail != null)
                                charactersResponse.value?.detail = body.detail
                            if (body.next != null)
                                charactersResponse.value?.next = body.next
                            if (body.previous != null)
                                charactersResponse.value?.previous = body.previous
                            if (body.results != null) {
                                charactersList.addAll(body.results as ArrayList<Result>)
                            }
                            if (body.next != null) {
                                val arr: Array<String> = charactersResponse.value!!.next!!.split("=").toTypedArray()
                                page.value = arr.last().toString().toInt()
                                fetchList()
                            }else if (body.next == null){
                                if (characters.value == null)
                                    characters.value = charactersList
                                else
                                    characters.value!!.addAll(charactersList)
                            }
                        }

                    } else {
                        charactersResponse.value?.detail = "Server is not responding"
                    }
                }

            }

        if (page.value == null){
            page.value = 1
        }
        Api.create().getStarWarsCharacters(page.value!!).enqueue(callback)
    }
}