package com.shamoon.starwarscharacters.service

import com.shamoon.starwarscharacters.service.model.StarWarsCharactersResponse
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.security.SecureRandom
import java.security.cert.X509Certificate
import javax.net.ssl.SSLContext
import javax.net.ssl.SSLSession
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager


interface Api {

    companion object {
        private const val BASE_URL = "https://swapi.dev/api/"
        private fun createTrustingOkHttpClient(): OkHttpClient? {
            return try {
                val x509TrustManager: X509TrustManager = object : X509TrustManager {
                    override fun checkClientTrusted(
                        chain: Array<X509Certificate?>?,
                        authType: String?
                    ) {
                    }

                    override fun checkServerTrusted(
                        chain: Array<X509Certificate?>?,
                        authType: String?
                    ) {
                    }

                    override fun getAcceptedIssuers(): Array<X509Certificate> {
                        return arrayOf()
                    }
                }
                val trustAllCerts: Array<TrustManager> = arrayOf(
                    x509TrustManager
                )
                val sslContext: SSLContext = SSLContext.getInstance("SSL")
                sslContext.init(null, trustAllCerts, SecureRandom())
                OkHttpClient.Builder()
                    .sslSocketFactory(sslContext.socketFactory, x509TrustManager)
                    .hostnameVerifier { hostname: String?, session: SSLSession? -> true }
                    .build()
            } catch (e: Exception) {
                throw RuntimeException(e)
            }
        }

        fun create(): Api {
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(createTrustingOkHttpClient())
                .build()
            return retrofit.create(Api::class.java)
        }
    }

    @GET("people")
    fun getStarWarsCharacters(@Query("page") page: Int): Call<StarWarsCharactersResponse>

}