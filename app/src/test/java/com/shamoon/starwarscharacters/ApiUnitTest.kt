package com.shamoon.starwarscharacters

import android.util.Log
import com.shamoon.starwarscharacters.service.Api
import org.junit.Assert
import org.junit.Test

class ApiUnitTest {
    @Test
    fun apiTest() {
        // call the api
        val api = Api.create("https://swapi.dev/api/")
        val response = api.getStarWarsCharacters(1).execute()
        // verify the response is OK
        Assert.assertTrue(response.code() == 200)
    }
}