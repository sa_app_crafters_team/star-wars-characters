# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Android application consisting of two screens for displaying
  information about Star Wars characters. The Star Wars character data should be obtained from
  the Star Wars API at https://swapi.dev/.

  The first screen have a list of character names. I displayed all available characters
  from the API. The app is an appropriate UI to indicate to the user that it is busy while
  it fetches results from the API. If the API request fails it displays an error message and a
  “Try Again” button which can be used to retry the API call.

  When the user taps on a character name the app switches to the second screen showing
  more detail about the selected character. The second screen shows labels and data values
  for each of the following character attributes:
  • Name
  • Height
  • Mass
  • The date and time that this character’s data record was created (in a
  human friendly format)

  The app have the following colour scheme for both screens:
  • Background: #222222
  • List item text: #FFFFFF
  • Label text: #DDBB00
  • Data value text: #FFFFFF

* Version 1.0
* Repository URL: https://shamoon192@bitbucket.org/shamoon192/star-wars-characters.git
* MVVM technical approach/design pattern is followed with data binding
* one Unit test is written named apiTest



### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact